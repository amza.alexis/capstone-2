const User = require('../models/userSchema');
const bcrypt = require('bcrypt');
const auth = require('../auth');


//Register Users and check if username && email already exist

	module.exports.registerUser = async (reqBody) => {

  	let inputEmail = await User.findOne({email: reqBody.email}).then(email => {

  		if(email === null) {
  			return true
  		} else {
  			return false
  		}
  	})

	let inputUsername = await User.findOne({username: reqBody.username}).then(username => {

  		if (username === null) {
  			
  			return true
  		} else {
  			return false
  		}
  	})

  	if(inputEmail === true ) {

		if(inputUsername === true) {

				let newUser = new User({

				username: reqBody.username,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				mobileNo: reqBody.mobileNo,
				address: reqBody.address
			})

			return newUser.save().then((user, err) => {
				if(err) {
					console.log(err)
					return false 
				} else {
					return (`Welcome ${user.username}!`)
				}
			})

		} else {
			return (`Username already exist!`)
		}

  	} else {
  		return (`Email already exist!`)
  	}
}

//login user

module.exports.loginUser = (reqBody) => {

	return User.findOne({username: reqBody.username}).then(result => {

		if(result === null) {

			

			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}

			} else {
				return false
			}
		}
	})
}

// setting an Admin account

module.exports.setupAdmin = (reqBody) => {

	return User.findOne({username: reqBody.username}).then(result => {

		if(result !== null) {
			result.isAdmin = true,
			result.password = ""
			return result

		} else false
	})

}

// get user details 

/*module.exports.getDetails = async (userData) => { 

	return User.findById({username: userData.username}).then((result, err) => {

		if(result !== null) {
			result.password = ""
			return result
		} else {
			console.log(err)
			return false
		}
	})


}*/