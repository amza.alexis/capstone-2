const express = require('express');
const auth = require('../auth');
const userController = require('../controllers/user');
const router = express.Router();


// register a user
router.post('/register', (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// login user
router.post('/login', (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// setup admin user
router.post('/admin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization) 

	userController.setupAdmin(req.body).then(resultFromController => res.send(resultFromController))
})

// get user details
/*router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	let data = {

		userId: userData.userId
	}

	userController.getDetails().then(resultFromController => res.send(resultFromController))
})*/

module.exports = router;
