const mongoose = require('mongoose');

const prodSchema = new mongoose.Schema({

	prodId: {
		type: String,
		required: [true, 'Product ID is required.']
	},

	name: {
		type: String,
		required: [true, 'Product name is required.']
	},

	description: {
		type: String,
		required: [true, 'description is required']
	},

	price: {
		type: String,
		required: [true, 'Price is required.']
	},

	qty: {
		type: Number,
		default: 0
	},

	isActive: {
		type: Boolean,
		default: true
	},

	orders: [
		{
			userId: {
				type: String,
				required: [true, 'User ID is required.']
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},

			qty: {
				type: Number,
				required: [true, 'Qty is required']
			}

		}]


})

module.exports = mongoose.model('Product', prodSchema)