const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	username: {
		type: String,
		required: [true, 'Username is required.']
	},

	email: {
		type: String,
		required: [true, 'Email is required.']
	},

	password: {
		type: String,
		required: [true, 'Password is required.']
	},

	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},

	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},

	mobileNo: {
		type: String,
		required: [true, 'Mobile number is required']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	address: {
		type: String,
		required: [true, 'Address is required.']
	},

	order: [
		{
			orderId: {
				type: String,
				required: [true, 'Order ID is required.']
			},

			prodName: {
				type: String,
				required: [true, 'Product name is required']
			},

			price: {
				type: Number,
				required: [true, 'Price is required.']
			},

			qty: {
				type: Number,
				required: [true, 'Quantity is required.']
			},

			orderedOn: {
				type: Date,
				default: new Date()
			},

			
		}],

	payment: [{

				isPaid: {
					type: Boolean,
					default: false
				},

				paidOn: {
					type: Date,
					default: new Date()
				},
				totalAmount: {
					type: Number,
					required: [true, 'Amount is required.']
				}
			}],

	shipment: [{

				isShipped: {
					type: Boolean,
					default: false
				},

				shippedOn: {
					type: Date,
					default: new Date()
				},

				courier: {
					type: String,
					required: [true, 'courier is required']
				}, 
			}]

})

module.exports = mongoose.model('User', userSchema);