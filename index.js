const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;
const userRoutes = require('./routers/user');

const app = express();

app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use(cors());

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.ag1bo.mongodb.net/capstone-2?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Connection Error"))
	db.once('open', () => console.log('Now connected to MongoDB Atlas'));

app.use('/user', userRoutes);

app.listen(port, () => console.log(`Server is running at port: ${port}.`))
